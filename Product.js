class Product {
    constructor(name, price, numberOfcopies) {
        this.name = name;
        this.price = price;
        this.numberOfcopies = numberOfcopies;
    }
    sellCopies() {
        console.log(`Name: ${this.name}`);
        console.log(`Price: ${this.price}`);
        console.log(`Number Of Copies: ${this.numberOfcopies}`);
    }
    orderCopies() {
        console.log(`Name: ${this.name}`);
        console.log(`Price: ${this.price}`);
        console.log(`Number Of Copies: ${this.numberOfcopies}`);
    }
}
export default Product;