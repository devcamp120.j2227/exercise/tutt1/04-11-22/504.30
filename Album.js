import Product from './Product.js';

class Album extends Product {
    constructor(name, price, numberOfcopies, artist) {
        super(name, price, numberOfcopies);
        this.artist = artist;
    }
    print() {
        console.log(this.artist);
    }
}

export default Album;