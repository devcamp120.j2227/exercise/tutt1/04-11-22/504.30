import Product from './Product.js';
import Album from './Album.js';
import Movie from './Movie.js';

let product = new Product('ABCD', 100.00, 1000);
product.sellCopies();
console.log(product instanceof Product);

let artist = new Album('ABCD', 100.00, 1000, 'Leonardo da Vinci');
artist.print();
console.log(artist instanceof Album);

let director = new Movie('ABCD', 100.00, 1000, 'John Doe');
director.print();
console.log(director instanceof Movie);