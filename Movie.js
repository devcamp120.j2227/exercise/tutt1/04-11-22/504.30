import Product from './Product.js';

class Movie extends Product {
    constructor(name, price, numberOfcopies, director) {
        super(name, price, numberOfcopies);
        this.director = director;
    }
    print() {
        console.log(this.director);
    }
}

export default Movie;